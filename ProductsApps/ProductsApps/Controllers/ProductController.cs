﻿using ProductsApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//Controler la doi tuong de xu ly cac request gui len
//Controler tiep nhan request gui len xy ly logic va tra ve response cho nhung request do
namespace ProductsApps.Controllers
{
    public class ProductController : ApiController
    {
        Product[] products = new Product[]
        {
            new Product{Id=1, Name="A",Category="HaNoi",Price=1.2M },
            new Product{Id=2, Name="B",Category="DaNang",Price=1.2M },
            new Product{Id=3, Name="C",Category="HCM",Price=1.2M }
        };
        //Method lay toan bo danh sach product
        public IEnumerable<Product> GetAllProducts()
        {
            //IEnumerable Là một mảng read-only, chỉ có thể đọc, không thể thêm hay bớt phần tử. Chỉ duyệt theo một chiều, từ đầu tới cuối mảng.
            return products;
        }
        //Method lay chi tiet 1 product theo ID
        public IHttpActionResult GetProducts(int Id)
        {
            //
            var product = products.FirstOrDefault(p => p.Id == Id);
            if(product == null){
                return NotFound();
            }
            return Ok(product);
        }
    }
}
