﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//Models la doi tuong dai dien cho du lieu trong ung dung
//ASP.NET WEB API co the tu dong serialize model thanh dinh dang JSON hay XML... va ghi DL serialize vao phan body cua response
namespace ProductsApps.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
    }
   
}