﻿CREATE DATABASE ProductApi

USE ProductApi
GO	

CREATE TABLE Product (
		ProductId UNIQUEIDENTIFIER PRIMARY KEY,
		Name NVARCHAR(100) NOT NULL,
		Title NVARCHAR(200) NOT NULL,
		Description NVARCHAR(1024) NOT NULL,
		Photo NVARCHAR(1024) NOT NULL,
		CategoryId UNIQUEIDENTIFIER NOT NULL,
		Price INT NOT NULL,
		Quantity INT NOT NULL,
		CreateOnDate DATETIME NOT NULL,
		CreateByUserId UNIQUEIDENTIFIER NOT NULL,
		LastModifiedOnDate DATETIME,
		LastModifiedBuyUserID UNIQUEIDENTIFIER,
		DisplayStatus INT,
)
INSERT INTO dbo.Product
        ( ProductId ,
          Name ,
          Title ,
          Description ,
          Photo ,
          CategoryId ,
          Price ,
          Quantity ,
          CreateOnDate ,
          CreateByUserId ,
          LastModifiedOnDate ,
          LastModifiedBuyUserID ,
          DisplayStatus
        )
VALUES  ( NEWID() , -- ProductId - uniqueidentifier
          N'Sản phẩm số 1' , -- Name - nvarchar(100)
          N'Sp 01 Title' , -- Title - nvarchar(200)
          N'Sp 01 Description' , -- Description - nvarchar(1024)
          N'Sp 01 Photo ' , -- Photo - nvarchar(1024)
          NEWID() , -- CategoryId - uniqueidentifier
          0 , -- Price - int
          0 , -- Quantity - int
          GETDATE() , -- CreateOnDate - datetime
          NEWID() , -- CreateByUserId - uniqueidentifier
          GETDATE() , -- LastModifiedOnDate - datetime
          NEWID() , -- LastModifiedBuyUserID - uniqueidentifier
          0  -- DisplayStatus - int
        )

INSERT INTO dbo.Product
        ( ProductId ,
          Name ,
          Title ,
          Description ,
          Photo ,
          CategoryId ,
          Price ,
          Quantity ,
          CreateOnDate ,
          CreateByUserId ,
          LastModifiedOnDate ,
          LastModifiedBuyUserID ,
          DisplayStatus
        )
VALUES  ( NEWID() , -- ProductId - uniqueidentifier
          N'Sản phẩm số 2' , -- Name - nvarchar(100)
          N'Sp 02 Title' , -- Title - nvarchar(200)
          N'Sp 02 Description' , -- Description - nvarchar(1024)
          N'Sp 02 Photo ' , -- Photo - nvarchar(1024)
          NEWID() , -- CategoryId - uniqueidentifier
          0 , -- Price - int
          0 , -- Quantity - int
          GETDATE() , -- CreateOnDate - datetime
          NEWID() , -- CreateByUserId - uniqueidentifier
          GETDATE() , -- LastModifiedOnDate - datetime
          NEWID() , -- LastModifiedBuyUserID - uniqueidentifier
          0  -- DisplayStatus - int
        )

INSERT INTO dbo.Product
        ( ProductId ,
          Name ,
          Title ,
          Description ,
          Photo ,
          CategoryId ,
          Price ,
          Quantity ,
          CreateOnDate ,
          CreateByUserId ,
          LastModifiedOnDate ,
          LastModifiedBuyUserID ,
          DisplayStatus
        )
VALUES  ( NEWID() , -- ProductId - uniqueidentifier
          N'Sản phẩm số 3' , -- Name - nvarchar(100)
          N'Sp 03 Title' , -- Title - nvarchar(200)
          N'Sp 03 Description' , -- Description - nvarchar(1024)
          N'Sp 03 Photo ' , -- Photo - nvarchar(1024)
          NEWID() , -- CategoryId - uniqueidentifier
          0 , -- Price - int
          0 , -- Quantity - int
          GETDATE() , -- CreateOnDate - datetime
          NEWID() , -- CreateByUserId - uniqueidentifier
          GETDATE() , -- LastModifiedOnDate - datetime
          NEWID() , -- LastModifiedBuyUserID - uniqueidentifier
          0  -- DisplayStatus - int
        )

SELECT * FROM dbo.Product