using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DatabaseFirstSample
{
    class Program
    {
        static BloggingContext db = new BloggingContext();
        static void Main(string[] args)
        {
            try
            {
                Them();
                Hienthi();
                Xoa();
                Hienthi();
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            } 
        }
        static void Hienthi()
        {
            // Hiển thị toàn bộ dữ liệu
            var query = from b in db.Blogs
                        orderby b.Name
                        select b;
            Console.WriteLine("All blogs in the database:");
            foreach (var item in query)
            {
                Console.WriteLine(item.Name);
            }
        }
        static void Them()
        {
            // Tạo mới và lưu dữ liệu
            Console.Write("Enter a name for a new Blog: ");
            var name = Console.ReadLine();
            var blog = new Blogs { Name = name };
            db.Blogs.Add(blog);
            db.SaveChanges();
        }
        static void Xoa()
        {
            //Console.Write("Delete blog with id: ");
            //int id = int.Parse(Console.ReadLine());
            Console.Write("Delete blog with name: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Blogs resuilt = db.Blogs.Where(p=>p.BlogId==id).FirstOrDefault();
            db.Blogs.Remove(resuilt);
            db.SaveChanges();
        }
    }
}
