using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CodeFirstNewDatabaseSample
{
    class Program
    {
        static BloggingContext db = new BloggingContext();
        static void Main(string[] args)
        {
            try
            {
                Them();
                HienThi();
                //Xoa();
                //HienThi();
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
           
           
        }
        static void Them()
        {
            // Tao moi va luu du lieu
            Console.Write("Enter a name and id for a new Blog: ");
            var name = Console.ReadLine();
            var blog = new Blog { Name = name };
            db.Blogs.Add(blog);
            db.SaveChanges();
        }
        static void HienThi()
        {
            // Hien thi toan bo du lieu
            var query = from b in db.Blogs
                        orderby b.Name
                        select b;
            Console.WriteLine("All blogs in the database:");
            foreach (var item in query)
            {
                Console.WriteLine(item.Name);
            }
        }
        static void Xoa()
        {
            Console.WriteLine("Enter a id");
            int id =Convert.ToInt32( Console.ReadLine());
            var item = db.Blogs.Where(p => p.BlogID == id).FirstOrDefault();
            db.Blogs.Remove(item);
            db.SaveChanges();
        }

    }
    public class Blog
    {
        public int BlogID { get; set; }
        public string Name { get; set; }

        public virtual List<Post> Posts { get; set; }

    }
    public class Post
    {
        public int PostID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Bl { get; set; }
        public virtual Blog Blog { get; set; }
    }
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
    }

}
